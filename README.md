# Django test app to submit a form

This is a simple Django app that let's the user fill a form
with basic information and a resume upload. All the fields
have proper validation, file_type and file_size restrictions
on file upload for instance. It has been developed using
Python 3.6.7 and Django 2.1.7. Other packages used are:
 
 - Django-Crispy-Forms
 - UK-postcode-utils
 - django-static-jquery
 
### Follow these instruction to set up and run the app

- Create a virtual environment (optional but highly recommended)
```bash
$ pip install virtualenv
$ virtualenv env_name
$ source /path/to/ENV/bin/activate
```

##### You have two options now:
###### Use the Makefile (easy)
For linux:
```bash
$ make all
```
For mac:
```bash
$ make all-mac
```

###### Run each command manually

- Install requirements
```bash
$ pip install -r requirements.txt
```

- Collect static files
```bash
$ python manage.py collectstatic
```

- Run migrations
```bash
$ python manage.py makemigrations
$ python manage.py migrate
```

- Run the app
```bash
$ python manage.py runserver
```
