.PHONY: clean system-packages python-packages install install-mac run django-commands all all-mac isort

clean:
	@find . -name '*.pyc' -delete
	@find . -name '*.log' -delete

system-packages:
	@sudo apt install python-pip -y

system-packages-mac:
	@sudo python -m ensurepip

python-packages:
	@pip install -r requirements.txt

install: system-packages python-packages

install-mac: system-packages-mac python-packages

isort:
    @sh -c "isort --skip-glob=.tox --recursive . "

django-commands:
	@python manage.py collectstatic <<<yes
	@python manage.py makemigrations
	@python manage.py migrate

run:
	@python manage.py runserver

all: clean install django-commands run

all-mac: clean install-mac django-commands run
