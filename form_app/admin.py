from django.contrib import admin

from form_app.models import EmployeeTestModel

admin.site.register(EmployeeTestModel)
