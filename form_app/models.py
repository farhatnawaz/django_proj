from django.core.validators import FileExtensionValidator
from django.db import models

from form_app.validators import validate_file_size


class EmployeeTestModel(models.Model):
    """This is the model which is used to save the form data into the DB"""

    full_name = models.CharField(max_length=50)
    post_code = models.CharField(max_length=8, blank=True)
    last_salary = models.IntegerField(default=0)
    uk_resident = models.BooleanField(default=False)
    resume_file = models.FileField(
        upload_to='media/',
        validators=[FileExtensionValidator(allowed_extensions=['pdf']), validate_file_size]
    )
