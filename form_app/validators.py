"""This module contains validators for this forms app"""
from django.core.exceptions import ValidationError


def validate_file_size(resume_file):
    """
    This method validates if the size of the file is within the permitted size
    :param resume_file: file to be validated
    :return:
    """
    file_size = resume_file.size
    if file_size > 5242880:
        raise ValidationError("The maximum file size that can be uploaded is 5MB")
    else:
        return resume_file
