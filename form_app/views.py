from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import View, FormView

from form_app.forms import EmployeeTestForm


class SubmissionSuccessView(View):
    """This view serves the success message when the form has been submitted"""

    template_name = 'form_app/success.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class EmployeeFormView(FormView):
    """This is the view that serves the form for the app"""

    form_class = EmployeeTestForm
    template_name = 'form_app/form.html'
    success_url = reverse_lazy('submission_success')

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(self.success_url)
        else:
            return render(request, self.template_name, {'form': form})
