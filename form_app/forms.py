from crispy_forms.helper import FormHelper
from django import forms
from ukpostcodeutils import validation

from form_app.models import EmployeeTestModel


class EmployeeTestForm(forms.ModelForm):
    error_css_class = 'error'

    class Meta:
        model = EmployeeTestModel
        fields = '__all__'
        widgets = {
            'full_name': forms.TextInput(attrs={'placeholder': u'Full Name'}),
        }
        help_texts = {
            'last_salary': 'Yearly, in GBP (£)'
        }

    def __init__(self, *args, **kwargs):
        super(EmployeeTestForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_show_labels = True
        self.helper.form_id = 'form-test'
        self.helper.form_class = 'uniForm'
        self.helper.form_method = 'POST'
        self.helper.form_action = '/forms/done'
        self.helper.form_show_errors = True
        self.helper.attrs = {'novalidate': True}

        self.fields['post_code'].widget.attrs = {'id': 'p_code', 'required': False}
        self.fields['uk_resident'].widget.attrs = {'onclick': 'showHidePostcode()'}
        self.fields['uk_resident'].label = 'Are you currently a UK resident?'

    def clean(self):
        """This method has been overridden to perform our custom validation on certain fields"""

        cleaned_data = super(EmployeeTestForm, self).clean()

        post_code = str(self.cleaned_data.get('post_code', None))
        if post_code:
            if not validation.is_valid_postcode(post_code.replace(' ', '')):
                self._errors["post_code"] = ["Please enter a valid UK postcode (eg NW7 1GH)"]
                del self.cleaned_data['post_code']

        full_name = str(cleaned_data.get('full_name', ''))
        if len(full_name.split(' ')) != 2:
            self._errors["full_name"] = ["Please enter a full name (first and last)"]
            del self.cleaned_data['full_name']

        salary = cleaned_data.get('last_salary', 0)
        if salary < 0:
            self._errors["last_salary"] = ["Please enter a value greater than or equal to 0"]
            del self.cleaned_data['last_salary']

        return cleaned_data
