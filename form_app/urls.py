from django.urls import path

from . import views

urlpatterns = [
    path('done', views.SubmissionSuccessView.as_view(), name='submission_success'),
    path('test_form', views.EmployeeFormView.as_view(), name='test_form')
]
